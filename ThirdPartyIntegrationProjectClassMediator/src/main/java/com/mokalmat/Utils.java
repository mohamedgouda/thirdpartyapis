package com.mokalmat;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.ByteArrayInputStream;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import org.w3c.dom.DOMException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

public class Utils extends AbstractMediator {

	public boolean mediate(MessageContext context) {
		log.info("Entering Utils Class ...");
		log.info((String) context.getProperty("ALLMESSAGE"));
		String allMessage = (String) context.getProperty("ALLMESSAGE");
		Document doc;
		String str = "";
		try {
			doc = stringToDom(allMessage);
			str = transformXML(doc);
			log.info(str);
		} catch (SAXException | ParserConfigurationException | IOException | TransformerException
				| FactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String operationType = (String) context.getProperty("OperationType");

		if (operationType.equalsIgnoreCase("BillEnquiry")) {
			log.info("Parse EFIN Bill Inquiry Response  ...");
			String parametarizedOutput = parseEFINBillEnquiryResponse(str);
			context.setProperty("parametarizedOutput", parametarizedOutput);
			log.info(parametarizedOutput);
		} else if (operationType.equalsIgnoreCase("CommissionEnquiry")) {
			String delimetedString = (String) context.getProperty("PARAMS");
			log.info(delimetedString);
			log.info("Parse EFIN Commission Inquiry Request  ...");
			String xmlString = parseRequest(allMessage, delimetedString, true);
			context.setProperty("XMLRequestMessage", xmlString);
			log.info(xmlString);
		} else if (operationType.equalsIgnoreCase("BillPayment")) {
			String delimetedString = (String) context.getProperty("PARAMS");
			log.info(delimetedString);
			log.info("Parse EFIN Bill Payment Request  ...");
			String xmlString = parseRequest(allMessage, delimetedString, false);
			context.setProperty("XMLRequestMessage", xmlString);
			log.info(xmlString);
		}
		return true;
	}

	private String parseRequest(String allMessage, String delimetedString, boolean isCommissions) {
		StringWriter writer = new StringWriter();
		try {

			InputStream is = new ByteArrayInputStream(allMessage.getBytes());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(is);
			doc.getDocumentElement().normalize();
			Element ParentElement;
			if (isCommissions) {
				ParentElement = (Element) doc.getElementsByTagName("FeeInqRq").item(0);
			} else {
				ParentElement = (Element) doc.getElementsByTagName("PmtInfo").item(0);
			}

			String[] inputArray = delimetedString.split(",");
			// Element FeeInqRq = rootElement.ge

			for (int i = 0; i < inputArray.length; i++) {
				String[] parameters = inputArray[i].split(":");
				Element PayAmt = doc.createElement("PayAmt");
				Element sequence = doc.createElement("Sequence");
				sequence.appendChild(doc.createTextNode(parameters[0]));
				Element Amt = doc.createElement("Amt");
				Amt.appendChild(doc.createTextNode(parameters[1]));
				Element CurCode = doc.createElement("CurCode");
				CurCode.appendChild(doc.createTextNode(parameters[2]));
				PayAmt.appendChild(sequence);
				PayAmt.appendChild(Amt);
				PayAmt.appendChild(CurCode);
				ParentElement.appendChild(PayAmt);
			}

			TransformerFactory tFact = TransformerFactory.newInstance();
			Transformer trans = tFact.newTransformer();
			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

			StreamResult result = new StreamResult(writer);
			DOMSource source = new DOMSource(doc);
			trans.transform(source, result);
			System.out.println(writer.toString());

		} catch (TransformerException ex) {
			System.out.println("Error outputting document");
		} catch (ParserConfigurationException ex) {
			System.out.println("Error building document");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return writer.toString();
	}

	private String parseEFINBillEnquiryResponse(String allMessage) {
		String returnString = "";
		try {

			InputStream is = new ByteArrayInputStream(allMessage.getBytes());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(is);

			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("CurAmt");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					if (returnString != "") {
						returnString = returnString + ",";
					}
					Element eElement = (Element) nNode;
					if (eElement.getElementsByTagName("ExactPmt").item(0).getTextContent().equalsIgnoreCase("true")) {
						returnString = returnString + eElement.getElementsByTagName("Sequence").item(0).getTextContent()
								+ ":" + eElement.getElementsByTagName("AmtDue").item(0).getTextContent() + ":"
								+ eElement.getElementsByTagName("CurCode").item(0).getTextContent();
					}
					System.out.println(returnString);
				}
			}
		} catch (IOException | ParserConfigurationException | DOMException | SAXException e) {
			e.printStackTrace();
		}
		return returnString;
	}

	private String transformXML(Document xml)
			throws TransformerException, ParserConfigurationException, FactoryConfigurationError {

		Source xmlSource = new DOMSource((Node) xml);
		StringWriter stringWriter = new StringWriter();
		Result result = new StreamResult(stringWriter);
		TransformerFactory transFact = TransformerFactory.newInstance();
		Transformer transformer = transFact.newTransformer();
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.INDENT, "no");
		transformer.transform(xmlSource, result);
		return stringWriter.getBuffer().toString();

		// return resultDoc;
	}

	private Document stringToDom(String xmlSource) throws SAXException, ParserConfigurationException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(new InputSource(new StringReader(xmlSource)));
	}

}
