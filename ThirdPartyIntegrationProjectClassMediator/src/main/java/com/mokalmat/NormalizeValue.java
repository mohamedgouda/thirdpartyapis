package com.mokalmat;

import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;
import java.util.Calendar;
import java.util.Date;
import java.text.Format;
import java.text.SimpleDateFormat;

public class NormalizeValue extends AbstractMediator {

	public boolean mediate(MessageContext context) {
		// TODO Implement your mediation logic here
		String value = (String) context.getProperty("OriginalValue");
		String operationType = (String) context.getProperty("OperationType");
		if (value == null || value.isEmpty()) {
			context.setProperty("CONVVALUE", null);
			return true;
		}

		if (operationType.compareToIgnoreCase("piasterstopound") == 0) {
			float convertedValue = fromPiastersToPount(Float.valueOf(value));
			context.setProperty("CONVVALUE", convertedValue);
		} else if (operationType.compareToIgnoreCase("poundtopiasters") == 0) {
			int convertedValue = fromPoundToPiasters(Float.valueOf(value));
			context.setProperty("CONVVALUE", convertedValue);
		}else if (operationType.compareToIgnoreCase("subtraction") == 0) {
			float totalBill = Float.valueOf(value);
			float commission = Float.valueOf((String) context.getProperty("Commission"));
			float convertedValue = totalBill - commission;
			context.setProperty("CONVVALUE", String.valueOf(convertedValue));
		}else if (operationType.compareToIgnoreCase("addyear") == 0) {
			Calendar date = Calendar.getInstance();
		    date.setTime(new Date());
		    Format f = new SimpleDateFormat("yyyyMMdd");
		    date.add(Calendar.YEAR,1);
			String convertedValue = f.format(date.getTime());
			context.setProperty("CONVVALUE", convertedValue);
		}else{
			context.setProperty("CONVVALUE", null);
		}
		return true;
	}

	private float fromPiastersToPount(float piasters) {

		return piasters / 100;
	}

	private int fromPoundToPiasters(float pound) {
		return Math.round(pound * 100);
	}
}