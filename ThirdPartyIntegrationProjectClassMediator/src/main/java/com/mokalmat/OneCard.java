package com.mokalmat;

import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

public class OneCard extends AbstractMediator {
	public boolean mediate(MessageContext context) {
		try {
			log.info("Entering OneCard Class ...");
			log.info("ConcString=" + context.getProperty("ConcString"));
			String ConcString = (String) context.getProperty("ConcString");
			if (ConcString.isEmpty()) {
				context.setProperty("md5String", "");
				log.info("Exiting");
				return true;
			}
			String md5String = getMD5(ConcString);
			context.setProperty("md5String", md5String);
			log.info(md5String);
		} catch (Exception exp) {
			log.error(exp.getStackTrace().toString());
		}
		return true;
	}

	private String getMD5(String md5) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		}
		return null;
	}
}
