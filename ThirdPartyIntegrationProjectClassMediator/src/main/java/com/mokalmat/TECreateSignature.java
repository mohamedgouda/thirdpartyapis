package com.mokalmat;

import org.apache.synapse.MessageContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.mediators.AbstractMediator;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class TECreateSignature extends AbstractMediator {

	private String salt;
	Log log= LogFactory.getLog(TECreateSignature.class);

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public boolean mediate(MessageContext context) {
		// TODO Implement your mediation logic here
		log.info("saltValue = " + getSalt());
		String signature = createSignature(context);
		context.setProperty("SIGNATURE", signature);
		context.setProperty("CONCVALUES", context.getProperty("concatenatedValues"));
		return true;
	}

	private String createSignature(MessageContext context) {
		String signature;
		try {
			StringBuilder sb = new StringBuilder();
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest((getSalt() + context.getProperty("concatenatedValues")).getBytes(StandardCharsets.UTF_8));
			for (byte b : hash) {
				sb.append(String.format("%02x", b));
			}
			signature = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			signature = null;
		}
		return signature;
	}
}
