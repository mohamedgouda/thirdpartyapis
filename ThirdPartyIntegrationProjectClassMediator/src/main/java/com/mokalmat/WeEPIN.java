package com.mokalmat;

import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class WeEPIN extends AbstractMediator {

	public boolean mediate(MessageContext context) {
		try{
		log.info("Entering WePIN Class ...");
		log.info("pinCodeByte=" + context.getProperty("pinCodeBytes"));
		String pinCodeString = (String) context.getProperty("pinCodeBytes");
		if (pinCodeString.isEmpty()) {
			context.setProperty("plainEPIN", "");
			log.info("Exiting");
			return true;
		}
		byte[] pinCodeBytes = hexStringToBytes(pinCodeString);
		String epinHex = "D32AA09E0CAB8A259FE6B19D0348CB9E053E9265B46F2ECDD0748C2B201C0268";
		byte[] epinKeyBytes = hexStringToBytes(epinHex);
		byte[] iv = Arrays.copyOfRange(pinCodeBytes, 0, 16);
		byte[] encryptedPIN2Decrypt = Arrays.copyOfRange(pinCodeBytes, 16, pinCodeBytes.length);
		byte[] plainTextPINBytes = decrypt(epinKeyBytes, iv, encryptedPIN2Decrypt);
		String plainEpin = bytesToText(plainTextPINBytes);
		context.setProperty("plainEPIN", plainEpin);
		log.info(plainEpin);
		}catch (Exception exp){
			log.error(exp.getStackTrace().toString());
		}
		return true;
	}

	private byte[] hexStringToBytes(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

	private byte[] decrypt(byte[] key, byte[] initVector, byte[] encrypted) {
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector);
			SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

			byte[] original = cipher.doFinal(encrypted);

			return original;
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
		}

		return null;
	}

	private String bytesToText(byte[] bytes) {
		StringBuilder PlaintextPinString = new StringBuilder();

		char[] tempchar = new char[bytes.length];
		for (int i = 0; i < bytes.length; i++) {
			tempchar[i] = (char) bytes[i];
			if (tempchar[i] >= 32 && tempchar[i] <= 126) {
				PlaintextPinString.append(tempchar[i]);
			}
		}
		return PlaintextPinString.toString();
	}

}
