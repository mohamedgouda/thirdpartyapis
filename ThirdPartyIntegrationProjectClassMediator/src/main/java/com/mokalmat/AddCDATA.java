package com.mokalmat;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMException;
import org.apache.axiom.om.OMNode;
import org.apache.axiom.om.OMText;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class AddCDATA extends AbstractMediator {

	private static final Log log = LogFactory.getLog(AddCDATA.class);

	public boolean mediate(MessageContext context) {

		log.info("Entering CDATA Class ...");
		log.info(context.getEnvelope().getBody().getFirstElement().getFirstElement().getFirstElement().toString());
		log.info((String) context.getProperty("ALLMESSAGE"));
		String operationType = (String) context.getProperty("OperationType");
		if (operationType.equalsIgnoreCase("AddCDATA")) {
			log.info("Add CDATA ...");
			String newXML = addCDATA(context);
			context.setProperty("CDATASection", newXML);
			log.info("Updated "+ newXML);
		} else if (operationType.equalsIgnoreCase("HandleCDATA")) {
			log.info("Handle CDATA ...");
			log.info(context.getEnvelope().getBody().getFirstElement().getFirstElement().getFirstElement().getText());
			try {
				String response = context.getEnvelope().getBody().getFirstElement().getFirstElement()
						.getFirstElement().getText();
				context.setProperty("ResponseMessage", response);
				Document doc = stringToDom(response);
				String str = transformXML(doc);
				context.setProperty("XMLResponseMessage", str);
				
				log.info("Updated "+ str);
			} catch (TransformerException | FactoryConfigurationError | OMException | SAXException
					| ParserConfigurationException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		log.info("Exiting CDATA Class ...");

		return true;
	}

	private String addCDATA(MessageContext context) {
		OMElement message = context.getEnvelope().getBody().getFirstElement().getFirstElement().getFirstElement();
		OMText newMessage = message.getOMFactory().createOMText(message, (String) context.getProperty("ALLMESSAGE"),
				OMNode.CDATA_SECTION_NODE);
		message.addChild(newMessage);
		return newMessage.toString();
	}

	private String transformXML(Document xml)
			throws TransformerException, ParserConfigurationException, FactoryConfigurationError {

		Source xmlSource = new DOMSource((Node) xml);
		StringWriter stringWriter = new StringWriter();
		Result result = new StreamResult(stringWriter);
		TransformerFactory transFact = TransformerFactory.newInstance();
		Transformer transformer = transFact.newTransformer();
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.INDENT, "no");
		transformer.transform(xmlSource, result);
		return stringWriter.getBuffer().toString();

		// return resultDoc;
	}

	private Document stringToDom(String xmlSource) throws SAXException, ParserConfigurationException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(new InputSource(new StringReader(xmlSource)));
	}

}

