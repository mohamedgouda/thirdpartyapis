package com.mokalmat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import sun.misc.BASE64Encoder;

public class EFinanceSignature extends AbstractMediator {
	private static final Log log = LogFactory.getLog(EFinanceSignature.class);
	public boolean mediate(MessageContext context) {
		String signature = EFINCreateSignature((String) context.getProperty("ALLMESSAGE"));
		log.info((String) context.getProperty("ALLMESSAGE"));
		log.info(signature);
		context.setProperty("SIGNATURE", signature);
		return true;
	}

	private InputStream file;

	private String EFINCreateSignature(String message) {

		String signedMessage = null;

		try {
			file = new FileInputStream("/opt/wso2ei-6.1.1/repository/resources/security/TestCert.pfx");
			Signature signSHA1RSA = Signature.getInstance("SHA1WithRSA");
			KeyStore keystore = KeyStore.getInstance("PKCS12");
			String alias = "73d96d2d-c155-4317-bbda-b1cd3c561d3b";
			keystore.load(file, "password".toCharArray());
			Key key = keystore.getKey(alias, "password".toCharArray());
			if (key instanceof PrivateKey) {
				signSHA1RSA.initSign((PrivateKey) key);
				signSHA1RSA.update((message).getBytes(StandardCharsets.UTF_8));
				byte[] signedMessageBytes = signSHA1RSA.sign();
				signedMessage = new BASE64Encoder().encode(signedMessageBytes);
			}
			return signedMessage;
		} catch (CertificateException | IOException | InvalidKeyException | KeyStoreException | NoSuchAlgorithmException
				| SignatureException | UnrecoverableKeyException ex) {
			return null;
		} finally {
			try {
				file.close();
			} catch (Exception ex) {

			}
		}
	}

}
